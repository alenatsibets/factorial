public abstract class Factorial {
    public static void main(String[] args) {
        int sum = 0;
    for (int i = 1; i <= 9;) {
        sum = sum + factorial(i);
        i = i + 2;
    }
        display(sum);
    }
    public static int factorial(int x){
        if (x == 1){
            return 1;
        }
        x = x * factorial(x - 1);
        return x;
    }
    public static void display(int sum){
        System.out.println("The sum of factorials of all odd numbers from 1 to 9: " + sum);
    }
}